# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Asta(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)
        
        window_before = self.driver.window_handles[0]
        
        main_page.click_on_element(page.MainPageElements.BUGGY_APP)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)

        main_page.click_on_element(page.MainPageElements.TASK_4)

        main_page.scroll_to_the_bottom()
        
        main_page.click_on_element(page.MainPageElements.APPLY_BUTTON)

        window_after_new = self.driver.window_handles[2]
        self.driver.switch_to_window(window_after_new)

        main_page.swith_to_iframe(page.MainPageElements.IFRAME_BUGGY)

        main_page.insert_name = 'Mariusz Brzoza'
        
        main_page.insert_email = 'qwer@dwr.pl'


        main_page.insert_phone = '100-200-300'

        main_page.click_on_element(page.MainPageElements.SAVE_BUTTON)
           

        search_results_page_mail = page.SearchResultsPage(self.driver)
        assert search_results_page_mail.is_email_correct(), "Zły format maila kiepie!!@@!"

        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_phone_correct(), "Zły format telefonu bączku"

        search_another_results_page = page.SearchResultsPage(self.driver)
        assert search_another_results_page.is_results_found(), "Cos nie działa kmiocie!!!11oneone!1!"

       
    def tearDown(self):
        time.sleep(2)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()