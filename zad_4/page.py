# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from faker import Faker
 
class InsertName(BasePageElement):

    locator = "//input[@id='name'][@name='name'][@placeholder='Imię i nazwisko']"

class InsertEmail(BasePageElement):

    locator = "//input[@id='name'][@name='email'][@placeholder='Email']"

class InsertPhone(BasePageElement):

    locator = "//input[@id='name'][@name='phone'][@placeholder='Poprawny format 600-100-200']"

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    REGULAR_APP = "//a[@class='button'][@data-action='bugfree']"
    BUGGY_APP = "//a[@class='button'][@data-action='buggy']"
    TASK_4 = "//div[@class='col-md-6' and ./a[@href='/task_4']]"
    APPLY_BUTTON = "//button[@class='btn btn-primary btn-block js-open-window']"
    IFRAME = "//iframe[@src='https://testingcup.pgs-soft.com/task_4_frame']"
    IFRAME_BUGGY = "//iframe[@src='https://buggy-testingcup.pgs-soft.com/task_4_frame']"
    SAVE_BUTTON = "//button[@id='save-btn']"

class MainPage(BasePage):

    insert_name = InsertName()
    insert_email = InsertEmail()
    insert_phone = InsertPhone()

    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()
           

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()

    def swith_to_iframe(self,xpath):

        iframes = self.driver.find_elements_by_xpath(xpath)
        self.driver.switch_to.frame(iframes[0])

    def scroll_to_the_bottom(self):

        self.driver.execute_script("window.scrollTo(0, 1000);")

    def select(self, xpath, value):
        select = Select(self.driver.find_element_by_xpath(xpath))
        select.select_by_value(value)

    def fake_data(self):
        self.fake_pl = Faker('pl_PL')
        self.fake_pl.seed(4321)


class SearchResultsPage(BasePage):

    def is_email_correct(self):
        return "Nieprawidłowy email" not in self.driver.page_source 
    
    def is_phone_correct(self):
        return "Zły format telefonu" not in self.driver.page_source  

    def is_results_found(self):
        return "Wiadomość została wysłana" in self.driver.page_source  

     