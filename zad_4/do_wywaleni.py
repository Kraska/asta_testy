import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.common.action_chains import ActionChains
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://asta.pgs-soft.com/")
        time.sleep(1)
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_4']]")
        element.click()
        time.sleep(1)
        driver.execute_script("window.scrollTo(0, 1000);")

        element = driver.find_element_by_xpath("//button[@class='btn btn-primary btn-block js-open-window']")
        element.click()
        time.sleep(1)
        window_after_new = driver.window_handles[2]
        
        time.sleep(1)
        
        driver.switch_to_window(window_after_new)
        time.sleep(5)
        element = driver.find_elements_by_xpath("//div")

        iframes = driver.find_elements_by_tag_name('iframe')
        driver.switch_to.frame(iframes[0])
        #iframes = driver.find_elements_by_xpath("//iframe[@src='https://testingcup.pgs-soft.com/task_4_frame']")
        #driver.switch_to.frame(iframes)
        
        print(len(element))
        print(len(iframes))
        element_nastepny = driver.find_element_by_xpath("//input[@id='name'][@name='name'][@placeholder='Imię i nazwisko']")
        element_nastepny.clear()
        element_nastepny.send_keys('abcdefghijklmnop')
        element_nastepny = driver.find_element_by_xpath("//input[@id='name'][@name='email'][@placeholder='Email']")
        element_nastepny.clear()
        element_nastepny.send_keys('mariusz@oned.du')
        element_nastepny = driver.find_element_by_xpath("//input[@id='name'][@name='phone'][@placeholder='Poprawny format 600-100-200']")
        element_nastepny.clear()
        element_nastepny.send_keys('100-200-300')
        element_nastepny = driver.find_element_by_xpath("//button[@id='save-btn']")
        time.sleep(1)
        element_nastepny.click()
        time.sleep(1000)
        


        

        
        assert "No results found." not in driver.page_source
        

    def tearDown(self):
        time.sleep(5)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()