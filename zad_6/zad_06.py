# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Asta(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)

        window_before = self.driver.window_handles[0]
        
        main_page.click_on_element(page.MainPageElements.BUGGY_APP)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)

        main_page.click_on_element(page.MainPageElements.TASK_6)
        
        main_page.insert_user_name = 'tester'

        main_page.insert_password = '123-xyz'
        
        main_page.click_on_element(page.MainPageElements.SUBMIT)
        
        main_page.click_on_element(page.MainPageElements.DOWNLOAD)
        
        
        

        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_results_found(), "Błędne dane logowania"

        
    def tearDown(self):
        time.sleep(2)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()