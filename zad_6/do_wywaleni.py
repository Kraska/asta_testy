import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://asta.pgs-soft.com/")
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_6/logged']]")
        element.click()
        time.sleep(2)
        
        element = driver.find_element_by_xpath("//input[@type='text'][@id='LoginForm__username']")
        element.send_keys ('tester')
        element = driver.find_element_by_xpath("//input[@type='password'][@id='LoginForm__password']")
        element.send_keys ('123-xyz')
        element = driver.find_element_by_xpath("//button[@type='submit']")
        element.click()
        element = driver.find_element_by_xpath("//a[@href='/task_6/download']")
        element.click()
        time.sleep(1)
        dirpath = os.getcwd()
        print("current directory is : " + dirpath)
        foldername = os.path.basename(dirpath)
        print("Directory name is : " + foldername)
        scriptpath = os.path.realpath(__file__)
        print("Script path is : " + scriptpath)
        time.sleep(3)

        #elem = driver.find_element_by_name("q")
        #elem.send_keys("pycon")
        #elem.send_keys(Keys.RETURN)
        assert "No results found." not in driver.page_source
        

    def tearDown(self):
        time.sleep(5)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()