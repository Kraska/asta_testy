# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from faker import Faker


class InsertName(BasePageElement):

    locator = "//input[@id='task8_form_name']"

class InsertCardNumber(BasePageElement):

    locator = "//input[@id='task8_form_cardNumber']"

class InsertCvvCode(BasePageElement):

    locator = "//input[@id='task8_form_cardCvv']"

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    REGULAR_APP = "//a[@class='button'][@data-action='bugfree']"
    TASK_8 = "//div[@class='col-md-6' and ./a[@href='/task_8']]"
    CARD_TYPE = "//select[@id='task8_form_cardType']"
    MONTH = "//select[@id='task8_form_cardInfo_month']"
    YEAR = "//select[@id='task8_form_cardInfo_year']"
    SAVE_BUTTON = "//button[@name='task8_form[save]']"

class MainPage(BasePage):

    insert_name = InsertName()
    insert_card_number = InsertCardNumber()
    insert_cvv_code = InsertCvvCode()
    
    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()
        

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()

    def select(self, xpath, value):
        select = Select(self.driver.find_element_by_xpath(xpath))
        select.select_by_value(value)

    def scroll_to_the_bottom(self):
        self.driver.execute_script("window.scrollTo(0, 1000);")

    def fake_data(self):
        self.fake_pl = Faker('pl_PL')
        self.fake_pl.seed(4321)


class SearchResultsPage(BasePage):
  
    def is_format_correct(self):
        return "Podaj wartość w wymaganym formacie" not in self.driver.page_source

    def is_cvv_correct(self):
        return "Nieprawidłowy format CVV." not in self.driver.page_source  

    def the_term_of_validity(self):
        return "Upłynął termin ważności karty" not in self.driver.page_source 

    def is_results_found(self):
        return "Zamówienie opłacone" in self.driver.page_source 