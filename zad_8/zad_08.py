# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Asta(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)

        
        window_before = self.driver.window_handles[0]
        
        main_page.click_on_element(page.MainPageElements.REGULAR_APP)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)

        main_page.click_on_element(page.MainPageElements.TASK_8)
                
        main_page.select(page.MainPageElements.CARD_TYPE, 'aec')

        main_page.insert_name = "Mariusz Brzoza"

        main_page.insert_card_number = 378734493671000

        main_page.insert_cvv_code = 123

        main_page.select(page.MainPageElements.MONTH, '02')

        main_page.select(page.MainPageElements.YEAR, '2022')

        main_page.scroll_to_the_bottom()

        main_page.scroll_and_click_on_element(page.MainPageElements.SAVE_BUTTON)


        
        time.sleep(1)
        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_format_correct(), "Zły format tej śmiesznej liczby. ZJEBAŁEŚ!"
        
        assert search_results_page.is_cvv_correct(), "Ten numerek tu nie pasuje, spróbuj ponownie. ZJEBAŁEŚ!"

        assert search_results_page.the_term_of_validity(), "Twoja karta jest zepsuta, wyrzuciłem ją na śmietnik, ZJEBAŁEŚ!"

        assert search_results_page.is_results_found(), "Wystąpił niespodziewany błąd, ZJEBAŁEŚ!"


        

    def tearDown(self):
        time.sleep(2)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()