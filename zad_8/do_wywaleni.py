import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.support.ui import Select
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://asta.pgs-soft.com/")
        nazwa_wyszukiwania = 'elektronika'
        time.sleep(1)
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_8']]")
        element.click()
        time.sleep(2)
        
        select = Select(driver.find_element_by_xpath("//select[@id='task8_form_cardType']]"))
        select.select_by_value(2)
        print(select)
        time.sleep(2)
        #element = driver.find_element_by_xpath("//input[@class='select2-search__field']")
        #element.send_keys (nazwa_wyszukiwania)
        #element.send_keys(Keys.RETURN)
        #element = driver.find_elements_by_xpath("//div[@class='col-sm-6 col-md-2']")
        
        #print("Ilosc rzeczy podczas wyszukiwania poprzez fraze '", nazwa_wyszukiwania, "' = " , len(element))

        
        assert "No results found." not in driver.page_source
        

    def tearDown(self):
        time.sleep(5)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()