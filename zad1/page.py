# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from faker import Faker


class InsertCityName(BasePageElement):

    locator = "//input[@class='form-control'][@name='User[billCity]']"

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    
    REGULAR_APP = "//a[@class='button'][@data-action='bugfree']"
    ZAD_1 = "//a[@href='/task_1']"
    COUNTRY = "//select[@class='form-control'][@name='User[billCountry]']"

class MainPage(BasePage):
    
    
    #insert_street_number = InsertStreetNumber()
   
    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()

    

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()

    def select(self, xpath, value):
        select = Select(self.driver.find_element_by_xpath(xpath))
        select.select_by_value(value)

    def fake_data(self):
        self.fake_pl = Faker('pl_PL')
        self.fake_pl.seed(4321)


class SearchResultsPage(BasePage):

    def is_results_found(self):
        return "uaktualnione" in self.driver.page_source  #<-jak to to takto?
