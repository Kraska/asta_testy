# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time


class Icongres(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)

        window_before = self.driver.window_handles[0]

        main_page.click_on_element(page.MainPageElements.REGULAR_APP) 
        time.sleep(2)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)
        
        main_page.click_on_element(page.MainPageElements.ZAD_1) 
        time.sleep(2)
        #search_results_page = page.SearchResultsPage(self.driver)
        #assert search_results_page.is_results_found(), "Błąd! Dane nie zostały zapisane"
        

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()