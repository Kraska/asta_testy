import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_search_in_python_org(self):
        driver = self.driver
        actionChains = ActionChains(driver)
        driver.get("https://asta.pgs-soft.com/")
        
        time.sleep(1)
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_9']]")
        element.click()
        time.sleep(2)
        
        element = driver.find_element_by_xpath("//li[@aria-labelledby='j1_1_anchor']/i[@class='jstree-icon jstree-ocl']")
        element.click()

        element = driver.find_element_by_xpath("//li[@aria-labelledby='j1_2_anchor']/i[@class='jstree-icon jstree-ocl']")
        element.click()
        
        
        element = driver.find_element_by_xpath("//a[@id='j1_3_anchor']")
        actionChains.context_click(element).perform()

        element = driver.find_element_by_xpath("//ul[@class='vakata-context jstree-contextmenu jstree-default-contextmenu']")
        element.click()
        time.sleep(1)

        delay = 100

        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//li[@aria-labelledby='j1_3_anchor']/a[1]")))
        myElem.send_keys('kekeke')
        myElem.send_keys(Keys.RETURN)

        #time.sleep(1)
        #element = driver.find_element_by_xpath("//ul[@class='vakata-context jstree-contextmenu jstree-default-contextmenu']")
        #element.click()
        #time.sleep(1)
        
        #elements = driver.find_elements_by_link_text('Child node 1-1')
        #elements = driver.find_elements_by_xpath("//*[contains(text(),'Child node 1-1')]")
        #element = driver.find_element_by_xpath("//li[@aria-labelledby='j1_3_anchor']/a[@id='j1_3_anchor']")
        #elements = driver.find_elements_by_xpath("//li[@aria-labelledby='j2_3_anchor']")
        #print(len(elements))
        #time.sleep(10)
        #element = elements[0]
        #element.send_keys('kekeke')
        
        #element.send_keys(Keys.RETURN)
        #element = driver.find_elements_by_xpath("//div[@class='col-sm-6 col-md-2']")
        
        
        assert "No results found." not in driver.page_source
        

    def tearDown(self):
        time.sleep(5)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()