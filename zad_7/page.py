# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select



class InsertName(BasePageElement):

    locator = "//input[@class='select2-search__field']"

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    REGULAR_APP = "//a[@class='button'][@data-action='bugfree']"
    TASK_7 = "//div[@class='col-md-6' and ./a[@href='/task_7']]"
    OBJECT_LIST = "//input[@type='number'][@min='0'][@step='1'][@class='form-control']"
    ELEMENT_TO_MOVE = "//img[@class='img-circle']"
    DESTINATION_POINT = "//div[@class='col-md-12 place-to-drop ui-droppable']"
    

class MainPage(BasePage):
    insert_name = InsertName()
    
    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()
        

    def select_object_from_list(self, xpath, value, number):
        elements = self.driver.find_elements_by_xpath(xpath)
        element = elements[value]
        element.clear()
        element.send_keys (number)

    def move_object(self, xpath, value, xpath2):
        elements = self.driver.find_elements_by_xpath(xpath)
        element = elements[value]
        destination_point = self.driver.find_element_by_xpath(xpath2)
        ActionChains(self.driver).drag_and_drop(element, destination_point).perform()

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()

    def select(self, xpath, value):
        select = Select(self.driver.find_element_by_xpath(xpath))
        select.select_by_value(value)

class SearchResultsPage(BasePage):
  
    def is_results_found(self):
        return "Łączna ilość produktów w koszyku nie może przekroczyć 100." not in self.driver.page_source