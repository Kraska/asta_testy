# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Asta(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)

        window_before = self.driver.window_handles[0]
        
        main_page.click_on_element(page.MainPageElements.REGULAR_APP)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)

        main_page.click_on_element(page.MainPageElements.TASK_7)

        item_number = random.randint(0,11)
        item_quantity = random.randint(0,250)
        main_page.select_object_from_list(page.MainPageElements.OBJECT_LIST,item_number,item_quantity)
        
        main_page.move_object(page.MainPageElements.ELEMENT_TO_MOVE,item_number,page.MainPageElements.DESTINATION_POINT)

        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_results_found(), "Za duzo rzeczy dodales kasztanie!"

        
    def tearDown(self):
        time.sleep(2)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()