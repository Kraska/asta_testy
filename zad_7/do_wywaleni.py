import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.common.action_chains import ActionChains
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(chrome_options=options)
        
    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://asta.pgs-soft.com/")
        
        time.sleep(1)
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_7']]")
        element.click()
        time.sleep(2)
        elements = driver.find_elements_by_xpath("//input[@type='number'][@min='0'][@step='1'][@class='form-control']")
        #elements = driver.find_elements_by_xpath("//*[contains(text(),'Kubek') and //input[@type='number'][@min='0'][@step='1'][@class='form-control']]")
        #elements = driver.find_elements_by_xpath("//span[@class='input-group-btn' and /input[@type='number']")
        print(type(elements))
        print(len(elements))
        
        element = elements[3]
        element.send_keys (9)
        time.sleep(2)
        element_do_przesuniecia = driver.find_elements_by_xpath("//img[@class='img-circle']")
        print(type(element_do_przesuniecia))
        print(len(element_do_przesuniecia))

        dupa = driver.find_elements_by_xpath("//*[contains(text(),'Kubek')]")
        print(len(dupa))

        element_do_przesuniecia_wlasciwy = element_do_przesuniecia[3]
        miejsce_docelowe = driver.find_element_by_xpath("//div[@class='panel-body']")
        #miejsce_docelowe = driver.find_element_by_xpath("//div[@class='col-md-12 place-to-drop ui-droppable'][@style='display: none;']")

        #element_do_przesuniecia = driver.find_elements_by_xpath("//div[@class='draggable ui-draggable ui-draggable-handle' and //*[contains(text(),'Okulary')]]")
        #miejsce_docelowe = driver.find_elements_by_xpath("//div[@class='col-md-12 place-to-drop ui-droppable']")

        ilosc_przeciagnij_i_upusc = driver.find_elements_by_xpath("//*[contains(text(), 'Łączna ilość produktów:')]")
        print("ilosc_przec_itd", len(ilosc_przeciagnij_i_upusc))

        
        #ActionChains(driver).drag_and_drop(element_do_przesuniecia_wlasciwy, ilosc_przeciagnij_i_upusc[1]).perform()

        print(ilosc_przeciagnij_i_upusc[0].location)
        print(ilosc_przeciagnij_i_upusc[0].size)
        
        #ActionChains(driver).click_and_hold(element_do_przesuniecia_wlasciwy).move_to_element(ilosc_przeciagnij_i_upusc[1]).release().perform()

        action = ActionChains(driver)
        action.drag_and_drop_by_offset(element_do_przesuniecia_wlasciwy, 1044, 358).perform()
        
        #elem = driver.find_element_by_name("q")
        #elem.send_keys("pycon")
        #elem.send_keys(Keys.RETURN)
        assert "No results found." not in driver.page_source
        

    def tearDown(self):
        time.sleep(2)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()