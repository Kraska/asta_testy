import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://asta.pgs-soft.com/")
        time.sleep(1)
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_5']]")
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//input[@type='file'][@accept='.csv, .txt']")
        #print(len(element))
        element.send_keys('C:\\Users\Stermedia\Desktop\Projekty\Selenium\Asta_tests\zad_5\Dane_klienta.txt')
        element.click()
        #element.send_keys(Keys.RETURN)
        
        assert "No results found." not in driver.page_source
        

    def tearDown(self):
        time.sleep(5)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()