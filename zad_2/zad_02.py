# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Asta(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)

        name = "elek"

        window_before = self.driver.window_handles[0]
        
        main_page.click_on_element(page.MainPageElements.BUGGY_APP)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)

        main_page.click_on_element(page.MainPageElements.TASK_2)
        
        main_page.click_on_element(page.MainPageElements.PLEASE_SELECT_CATEGORY)
           
        main_page.insert_name = name

        main_page.check_if_group_is_correct(page.MainPageElements.GROUP_NAME, name)

        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_results_found(), "Brak przedmiotów o danej nazwie"

        print("Ilosc rzeczy podczas wyszukiwania poprzez fraze '", name, "' = " , len(self.driver.find_elements_by_xpath(page.MainPageElements.QUANTITY_OF_ELEMENTS)))


    def tearDown(self):
        time.sleep(2)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()