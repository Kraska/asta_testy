import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://asta.pgs-soft.com/")
        time.sleep(1)
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        time.sleep(1)
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_1']]")
        element.click()
        time.sleep(1)
        elements = driver.find_elements_by_xpath("//div[@class='caption']")
        print(len(elements))
        time.sleep(1)
        element = elements[2]
        #print(element.text['Cena:':])
        start = 'Cena: '
        end = ' zł'
        suma = 0
        for i in range(5):
            elementalist = float(element.text[element.text.find(start)+len(start):element.text.find(end)])
            suma = suma + elementalist
            print(suma)
        #elem = driver.find_element_by_name("q")
        #elem.send_keys("pycon")
        #elem.send_keys(Keys.RETURN)
        #assert "No results found." not in driver.page_source


    def tearDown(self):
        time.sleep(5)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()