# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Asta(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)

        window_before = self.driver.window_handles[0]
        
        main_page.click_on_element(page.MainPageElements.REGULAR_APP)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)

        main_page.click_on_element(page.MainPageElements.TASK_1)
        
        x = 0 
        all_item_quantity = 0
        all_item_value = 0
        for i in range(10):
            
            item_number = random.randint(0,11)
            item_quantity = random.randint(1,14)
    
            main_page.select_object_from_list(page.MainPageElements.OBJECT_LIST,item_number,item_quantity)
            
            x = main_page.send_object_price_from_list(page.MainPageElements.ITEM_VALUE,item_number)
            
            main_page.click_after_send_object(page.MainPageElements.ADD_ITEM,item_number)

            search_results_page = page.SearchResultsPage(self.driver)
            assert search_results_page.is_results_found(), "Za duzo rzeczy dodales kasztanie!"

            r = main_page.check_total_price()

            w = main_page.check_total_quantity(page.MainPageElements.SUMMARY_ITEM_QUANTITY)
            print(x)
        
            all_item_quantity += item_quantity
            all_item_value = round(all_item_value + x * item_quantity,2)
            search_results_page = page.SearchResultsPage(self.driver)

            assert search_results_page.is_results_found(), "Za duzo rzeczy dodales kasztanie!"
            #print('ilosc pierwsza', all_item_value, 'ilosc druga', r)
            assert  all_item_quantity == w, "nie równa się ilosc przedmiotów XD"

            assert  all_item_value == r, "nie równa się koszt wszytkich przedmiotów, joł XD"
                  

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()