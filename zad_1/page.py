# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from faker import Faker
import random

class InsertItemsQuantity(BasePageElement):

    locator = "//input[@type='number'][@min='0'][@step='1'][@class='form-control']"


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    REGULAR_APP = "//a[@class='button'][@data-action='bugfree']"
    BUGGY_APP = "//a[@class='button'][@data-action='buggy']"
    TASK_1 = "//div[@class='col-md-6' and ./a[@href='/task_1']]"
    OBJECT_LIST = "//input[@type='number'][@min='0'][@step='1'][@class='form-control']"
    ADD_ITEM = "//span[@class='input-group-btn']"
    ITEM_VALUE = "//div[@class='caption']"
    SUMMARY_PRICE = "//span[@class='summary-price']"
    SUMMARY_ITEM_QUANTITY = "//span[@class='summary-quantity']"

    

class MainPage(BasePage):

    insert_items_quantity = InsertItemsQuantity()
    
    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()
        

    

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()
    
    def select_object_from_list(self, xpath, value, number):
        elements = self.driver.find_elements_by_xpath(xpath)
        element = elements[value]
        element.clear()
        element.send_keys (number)

    def send_object_price_from_list(self, xpath, value):
        elements = self.driver.find_elements_by_xpath(xpath)
        element = elements[value]
        start = 'Cena: '
        end = ' zł'
        element_value = float(element.text[element.text.find(start)+len(start):element.text.find(end)])
        return element_value
        
    def check_total_price(self):
        element = self.driver.find_element_by_xpath(MainPageElements.SUMMARY_PRICE)
        end = ' zł'
        element_value = float(element.text[:element.text.find(end)])
        return element_value

    def check_total_quantity(self, xpath):
        element = self.driver.find_element_by_xpath(xpath)
        return int(element.text)
        


           
    def click_after_send_object(self, xpath, value):
        elements = self.driver.find_elements_by_xpath(xpath)
        element = elements[value]
        element.click()


    
    def fake_data(self):
        self.fake_pl = Faker('pl_PL')
        self.fake_pl.seed(4321)


class SearchResultsPage(BasePage):
  
    def is_results_found(self):
        return "Komunikat ze strony testingcup.pgs" not in self.driver.page_source 
