# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Asta(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://asta.pgs-soft.com/")

    def test_data_update(self):
        
        main_page = page.MainPage(self.driver)
        
        window_before = self.driver.window_handles[0]
        
        main_page.click_on_element(page.MainPageElements.BUGGY_APP)

        window_after = self.driver.window_handles[1]
        self.driver.switch_to_window(window_after)

        main_page.click_on_element(page.MainPageElements.TASK_3)
        
        main_page.click_on_element(page.MainPageElements.MENU_BUTTON)
        
        main_page.click_on_element(page.MainPageElements.FORM_BUTTON)

        main_page.click_on_element(page.MainPageElements.EDIT_MODE_BUTTON)
        
        main_page.insert_name = 'Mariusz'
        
        main_page.insert_surname = 'Brzoza'
        
        main_page.insert_notes = 'Potężna wichura łamiąc duże drzewa, trzciną zaledwie tylko kołysze'
        
        main_page.insert_phone = '0700880712'

        dirpath = os.getcwd()
        main_page.insert_picture = dirpath +'\krowa.jpg'
        
        

        main_page.click_on_element(page.MainPageElements.SAVE_BUTTON)

        time.sleep(5)

        search_results_page = page.SearchResultsPage(self.driver)

        assert search_results_page.is_file_correct(), "Zły format dodanego pliku"

        assert search_results_page.is_results_found(), "Cos nie działa kmiocie!!!11oneone!1!, dane nie zostały zapisane"

        

       
    def tearDown(self):
        time.sleep(2)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()