import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
#from faker import Faker

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("https://asta.pgs-soft.com/")
        time.sleep(1)
        window_before = driver.window_handles[0]
        element = driver.find_element_by_xpath("//a[@class='button'][@data-action='bugfree']")
        element.click()
        time.sleep(1)
        window_after = driver.window_handles[1]
        driver.switch_to_window(window_after)
        element = driver.find_element_by_xpath("//div[@class='col-md-6' and ./a[@href='/task_3']]")
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//a[@data-toggle='dropdown']")
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//b[@class='caret-right']")
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//a[@id='start-edit']")
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//input[@id='in-name']")
        element.clear()
        element.send_keys('dudududupa')
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//input[@id='in-surname']")
        element.clear()
        element.send_keys('Brzozowska')
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//textarea[@id='in-notes']")
        element.clear()
        element.send_keys('jak sie najesz tak sie zesrasz')
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//input[@id='in-phone']")
        element.clear()
        element.send_keys('007328500')
        element.click()
        time.sleep(1)
        element = driver.find_element_by_xpath("//input[@id='in-file']")
        element.send_keys('C:\\Users\Stermedia\Desktop\Projekty\Selenium\Asta_tests\zad_3\krowa.jpg')
        element.click()

        #element = driver.find_element_by_xpath("//input[@id='in-name']")
        
        #element.send_keys("dupa")
        
        #element.send_keys(Keys.RETURN)

        #elem = driver.find_element_by_name("q")
        #elem.send_keys("pycon")
        #elem.send_keys(Keys.RETURN)
        assert "No results found." not in driver.page_source
        

    def tearDown(self):
        time.sleep(5)
        self.driver.close()

if __name__ == "__main__":
    unittest.main()