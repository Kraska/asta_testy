# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from faker import Faker


class InsertName(BasePageElement):

    locator = "//input[@id='in-name']"

class InsertSurname(BasePageElement):

    locator = "//input[@id='in-surname']"

class InsertNotes(BasePageElement):

    locator = "//textarea[@id='in-notes']"

class InsertPhone(BasePageElement):

    locator = "//input[@id='in-phone']"

class InsertPicture(BasePageElement):

    locator = "//input[@id='in-file']"

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    REGULAR_APP = "//a[@class='button'][@data-action='bugfree']"
    BUGGY_APP = "//a[@class='button'][@data-action='buggy']"
    TASK_3 = "//div[@class='col-md-6' and ./a[@href='/task_3']]"
    PLEASE_SELECT_CATEGORY = "//span[@class='select2-selection select2-selection--single']"
    MENU_BUTTON = "//a[@data-toggle='dropdown']"
    FORM_BUTTON = "//b[@class='caret-right']"
    EDIT_MODE_BUTTON = "//a[@id='start-edit']"
    SAVE_BUTTON = "//button[@id='save-btn']"


class MainPage(BasePage):
    insert_name = InsertName()
    insert_surname = InsertSurname()
    insert_notes = InsertNotes()
    insert_phone = InsertPhone()
    insert_picture = InsertPicture()
    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()
        

    

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()

    def select(self, xpath, value):
        select = Select(self.driver.find_element_by_xpath(xpath))
        select.select_by_value(value)

    def fake_data(self):
        self.fake_pl = Faker('pl_PL')
        self.fake_pl.seed(4321)


class SearchResultsPage(BasePage):
  
    def is_results_found(self):
        return "Twoje dane zosta" in self.driver.page_source 

    def is_file_correct(self):
        return "Zły format pliku" not in self.driver.page_source 
